import type { IConfig } from '../../autoinstallers/rush-plugins/node_modules/rush-init-project-plugin';

const config: IConfig = {
  prompts: [],
  plugins: [],
  defaultProjectConfiguration: {
    reviewCategory: 'service'
  }
};

export default config;
