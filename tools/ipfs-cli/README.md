# IPFS CLI


Upload the file or a directory in a secure way to any IPFS node that is secured by x-api-key header. 

```bash
AN_IPFS_API_URL="https://3000-kelpdigital-oss-rsg3ao46o68.ws-eu64.gitpod.io/ipfs_api/v0" pnpm serve -- add ../../rush.json
```
