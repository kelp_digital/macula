// https://github.com/sveltejs/kit/pull/6446
export const ssr = false;
export const csr = true;
export const prerender = true;
export const trailingSlash = 'always';
