# Change Log - @kelp_digital/sveltekit-adapter-macula

This log was last generated on Wed, 25 Jan 2023 12:46:35 GMT and should not be manually modified.

## 0.2.0
Wed, 25 Jan 2023 12:46:35 GMT

### Minor changes

- stable sveltekit, fix the code to reflect the changes

