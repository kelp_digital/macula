# @kelp_digital/sveltekit-adapter-macula

> HEAVY WORK IN PROGRESS. USE AT YOUR OWN RISK

Sveltekit adapter for [macula](https://macula.link)

The adapter will create the manifest and proper build for hosting with the Macula hosting solution. Think like nextjs or netlify but free and open source.

## Development
