/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-explicit-any */
import type { Adapter, Builder } from '@sveltejs/kit';
import { BuildOptions } from 'esbuild';
import { readFileSync } from 'fs';
import { stat } from 'fs/promises';
import path from 'path';
import { isEmpty, isNil, startsWith } from 'ramda';

import { ManifestV3 } from './manifest-v3.js';
import {
  adaptInlineScript,
  adaptLinksInHtmlFiles,
  adaptManifestFile,
  compileContentScripts,
  compileServiceWorker,
  compileWebAccessibleResources,
  IManifestCompilation
} from './utils.js';

// we are compiling to ESM only, so this is OK

/**
 * Adapter options
 * @internal
 */
interface IAdapterOptions {
  manifest: string;
  precompress?: boolean;
  baseDirectory: string;
  outDirectory?: string;
  esbuildOptions?: BuildOptions;
  trailingSlash: 'never' | 'always';
  fallback?: boolean;
}

/**
 * Macula adapter
 * @param incomingOption -
 * @public
 * @returns
 */
export default function (incomingOption: IAdapterOptions): Adapter {
  const adapter: Adapter = {
    name: 'sveltekit-adapter-chrome-extension',
    async adapt(builder: Builder) {
      const {
        log,
        config: { kit }
      } = builder;

      const {
        manifest,
        precompress = true,
        outDirectory = 'dist',
        baseDirectory,
        esbuildOptions = {},
        trailingSlash = 'never',
        fallback = false
      } = incomingOption;

      if (isNil(manifest) || isEmpty(manifest)) {
        log.error('Manifest must not be undefined or empty');
        return;
      } else {
        try {
          await stat(manifest);
        } catch (error) {
          log.error(error);
          return;
        }
      }

      /**
       * chrome is complaining about the directory called _app
       * so usually this would be app, but better to notify people not to use underscore at all
       */
      if (startsWith('_', kit.appDir)) {
        log.error(`kit.appDir must not start with _ please change it to a string that doesn't start with _`);
        return;
      }

      ///// svelte kit normal stuff

      builder.rimraf(outDirectory);

      if (!fallback) {
        //         const dynamic_routes: string[] = [];
        //         // this is a bit of a hack — it allows us to know whether there are dynamic
        //         // (i.e. prerender = false/'auto') routes without having dedicated API
        //         // surface area for it
        //         builder.createEntries((route) => {
        //           dynamic_routes.push(route.id);
        //           return {
        //             id: '',
        //             filter: () => false,
        //             complete: () => {}
        //           };
        //         });
        //         if (dynamic_routes.length > 0) {
        //           const prefix = path.relative('.', builder.config.kit.files.routes);
        //           const has_param_routes = dynamic_routes.some((route) => route.includes('['));
        //           const config_option =
        //             has_param_routes || JSON.stringify(builder.config.kit.prerender.entries) !== '["*"]'
        //               ? `  - adjust the \`prerender.entries\` config option ${
        //                   has_param_routes ? '(routes with parameters are not part of entry points by default)' : ''
        //                 } — see https://kit.svelte.dev/docs/configuration#prerender for more info.`
        //               : '';
        //           builder.log.error(
        //             `@sveltejs/adapter-static: all routes must be fully prerenderable, but found the following routes that are dynamic:
        // ${dynamic_routes.map((id) => `  - ${path.posix.join(prefix, id)}`).join('\n')}
        // You have the following options:
        //   - set the \`fallback\` option — see https://github.com/sveltejs/kit/tree/master/packages/adapter-static#spa-mode for more info.
        //   - add \`export const prerender = true\` to your root \`+layout.js/.ts\` or \`+layout.server.js/.ts\` file. This will try to prerender all pages.
        //   - add \`export const prerender = true\` to any \`+server.js/ts\` files that are not fetched by page \`load\` functions.
        // ${config_option}
        //   - pass \`strict: false\` to \`adapter-static\` to ignore this error. Only do this if you are sure you don't need the routes in question in your final app, as they will be unavailable. See https://github.com/sveltejs/kit/tree/master/packages/adapter-static#strict for more info.
        // If this doesn't help, you may need to use a different adapter. @sveltejs/adapter-static can only be used for sites that don't need a server for dynamic rendering, and can run on just a static file server.
        // See https://kit.svelte.dev/docs/page-options#prerender for more details`
        //           );
        //           throw new Error('Encountered dynamic routes');
        //         }
      } else {
        builder.generateFallback(path.join(outDirectory, 'index.html'));
      }

      builder.writeClient(outDirectory);
      builder.writePrerendered(outDirectory);

      // console.log('builder.config.kit.files', builder.config.kit.files);

      if (precompress) {
        log.minor('Compressing assets and pages');
        await builder.compress(outDirectory);
      }

      log(`Wrote site to "${outDirectory}"`);

      ///// END svelte kit normal stuff

      ///// START chrome extension specific processing
      log('Starting with the compilation of manifest assets ...');

      const manifestImport = JSON.parse(readFileSync(manifest).toString()) as ManifestV3;

      const commonCompileOptions: IManifestCompilation = {
        baseDirectory,
        outDirectory,
        manifest: manifestImport,
        builder,
        esbuildOptions
      };

      const sw = await compileServiceWorker(commonCompileOptions);
      const cs = await compileContentScripts(commonCompileOptions);
      const war = await compileWebAccessibleResources(commonCompileOptions);

      const manifestOverrides = manifestImport;

      if (!isNil(sw) && !isEmpty(sw)) {
        manifestOverrides.background = sw;
      }

      if (!isNil(cs) && !isEmpty(cs)) {
        manifestOverrides.content_scripts = cs;
      }

      if (!isNil(war) && !isEmpty(war)) {
        manifestOverrides.web_accessible_resources = war;
      }

      await adaptManifestFile(outDirectory, manifestImport, manifestOverrides);

      await adaptLinksInHtmlFiles(outDirectory, trailingSlash, log);

      await adaptInlineScript(outDirectory, log);

      ///// chrome extension specific processing
    }
  };

  return adapter;
}
