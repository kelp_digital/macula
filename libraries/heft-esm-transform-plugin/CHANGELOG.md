# Change Log - @kelp_digital/heft-esm-transform-plugin

This log was last generated on Wed, 25 Jan 2023 12:46:35 GMT and should not be manually modified.

## 0.2.0
Wed, 25 Jan 2023 12:46:35 GMT

### Minor changes

- init

### Patches

- init the features
- rename the internal signature, add commonjs optional and nullable
- rename internal method
- write file is sync now

