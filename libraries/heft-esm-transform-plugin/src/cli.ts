import { readFile } from 'fs/promises';
import { resolve } from 'path';

import { modifyFiles } from './modify';
import { createPackageJsonFile } from './packageJson';

async function main(): Promise<void> {
  const packageJsonPath: string = resolve(process.cwd(), '../package.json');
  const { name } = JSON.parse((await readFile(packageJsonPath)).toString());

  const esmLibPath: string = resolve(process.cwd(), '../lib');
  const cjsLibPath: string = resolve(process.cwd(), '../lib-commonjs');

  await modifyFiles(esmLibPath);

  await createPackageJsonFile(esmLibPath, 'module', name);
  await createPackageJsonFile(cjsLibPath, 'commonjs', name);
}
main()
  .then(() => console.log('DONE!!'))
  .catch(console.error);
