/**
 * Make a json string for the package.json file
 * @param pgkType -
 * @param name -
 * @returns
 */

import { writeFileSync } from 'fs';

export function makePackageJsonStringFor(pgkType: 'module' | 'commonjs', name: string): string {
  return JSON.stringify(
    {
      name,
      type: pgkType
    },
    null,
    2
  );
}

/**
 * Create package.json file for given type
 * @param dirPath -
 * @param pgkType -
 * @param name -
 */
export async function createPackageJsonFile(
  dirPath: string,
  pgkType: 'module' | 'commonjs',
  name: string
): Promise<void> {
  const cjsPkgJsonPath = `${dirPath}/package.json`;
  writeFileSync(cjsPkgJsonPath, makePackageJsonStringFor(pgkType, name));
}
