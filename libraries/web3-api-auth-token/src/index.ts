// Copyright (c) Kelp Digital OU. Full license is located in $root/LICENSE

/**
 * A library for creating and validating the Web3 API Auth Tokens or WAATs
 *
 * @packageDocumentation
 */
export * from './middleware/expressV4AuthMiddleware';
export * from './strategies';
export * from './strategies/errors';
export * from './utils/base64url';

// import * as expressV4AuthMiddleware from './middleware/expressV4AuthMiddleware';
// import * as strategies from './strategies';
// import * as errors from './strategies/errors';
// import * as base64url from './utils/base64url';

// export { base64url, errors, expressV4AuthMiddleware, strategies };
