# @kelp_digital/heft-library-rig

> HEAVY  WORK IN PROGRESS. USE AT YOUR OWN RISK

This rig brings modern settings for your library, web app or microservice. Most enterprises like to work with LTS systems and battle-tested packages, which tends to impact the build and transpiling process. CommonJS is not a bad practice, but let’s be honest, its time is slowly fading away. ESM is the future. 

This rig ships with two profiles, a default which is the ESM first then CommonJS, and pure ESM.


Profiles:

`default` or pure ESM:

- `api-extractor.json`
- `heft.json`
- `jest.config.json`
- `typescript.json` configured to emit the ESM and CJS 

`cjs` or CommonJS:

- `api-extractor.json`
- `heft.json`
- `jest.config.json`
- `typescript.json` configured to emit the ESM and CJS 

`without-extractor`:

- `heft.json`
- `jest.config.json`
- `typescript.json` configured to emit the ESM and CJS 

